package il.technion.cs236369.webserver.ISimpleServlet;

public interface HttpSimpleSession {
	
	Object getAttribute(String name);
	
	void setAttribute(String name, Object value);
	
	void invalidate();
}
