package il.technion.cs236369.webserver.ISimpleServlet;

public interface HttpSimpleServletRequest {

	String getHeader(String name);
	
	String getParameter(String name);
	
	HttpSimpleSession getSession();
}
